name 'consul-platform'
maintainer 'Chef Platform'
maintainer_email 'incoming+chef-platform/consul-platform@'\
  'incoming.gitlab.com'
license 'Apache-2.0'
description 'Install and Configure a Consul cluster'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
source_url 'https://gitlab.com/chef-platform/consul-platform'
issues_url 'https://gitlab.com/chef-platform/consul-platform/issues'
version '1.2.0'

chef_version '>= 12.14'

supports 'centos', '>= 7.1'

depends 'cluster-search'
depends 'ark'
